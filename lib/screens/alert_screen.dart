import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AlertScreen extends StatelessWidget {
   
  const AlertScreen({Key? key}) : super(key: key);

  void displayDialogIOS(BuildContext context) {
    showCupertinoDialog(
      barrierDismissible: true,
      context: context, 
      builder: (context) {
        return CupertinoAlertDialog(
          title: const Text("Title"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: const[
              Text("Este es el contenido de la alerta caslknc ascl nkñascasñ lknclñkn"),
              SizedBox(height: 20,),
              FlutterLogo(size: 100,)
            ],
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Cancelar")
            ),
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Cancelar")
            )
          ],
        );
      },
    );
  }

  void displayDialogAndroid(BuildContext context) {
    showDialog(
      barrierDismissible: true,
      context: context, 
      builder: (context) =>
        AlertDialog(
          elevation: 5,
          title: const Text("Titulo de la alerta"),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20)
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Cancelar")
              )
          ],
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: const[
              Text("Este es el contenido de la alerta caslknc ascl nkñascasñ lknclñkn"),
              SizedBox(height: 20,),
              FlutterLogo(size: 100,)
            ],
          ),
        ),
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
         child: ElevatedButton(
           child: const Text("MOSTRAR ALERTA"),
           onPressed: () => displayDialogIOS(context),
         ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.close),
        onPressed: () {
          // Navigator.pop(context);
        },
      ),
    );
  }
} 