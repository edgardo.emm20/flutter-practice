import 'package:flutter/material.dart';

class Listview2Screen extends StatelessWidget {
   
  const Listview2Screen({Key? key}) : super(key: key);

  final options = const['Megaman', 'Metal Gear', 'megaman'];
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Listview tipo 1"),
      ),
      body: ListView.separated(
        itemBuilder:  (context, index) {
          return ListTile(
            title: Text(options[index]),
            leading: const Icon(Icons.ac_unit_outlined),
            trailing: const Icon(Icons.arrow_right_sharp ),
            onTap: () {
              final game = options[index];
              print(game);
            },
          );
        }, 
        separatorBuilder: (_, __) => const Divider(),
        itemCount: options.length
      )
    );
  }
}