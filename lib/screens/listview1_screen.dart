import 'package:flutter/material.dart';

class Listview1Screen extends StatelessWidget {
   
  const Listview1Screen({Key? key}) : super(key: key);

  final options = const['Megaman', 'Metal Gear'];
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Listview tipo 1")
      ),
      body: ListView(
        children: [
          ...options.map((e) {
            return ListTile(
              title: Text(e),
              leading: const Icon(Icons.ac_unit_outlined),
              trailing: const Icon(Icons.arrow_forward_ios_sharp),
            );
          }).toList()
          // ListTile(
          //   title: Text("Hola mundo"),
          //   leading: Icon(Icons.ac_unit_outlined),
          // )
        ],
      )
    );
  }
}