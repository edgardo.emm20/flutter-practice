import 'package:flutter/material.dart';

class AvatarScreen extends StatelessWidget {
   
  const AvatarScreen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Avatar scrren"),
        actions: [
          Container(
            margin: const EdgeInsets.only(right: 20),
            child: const CircleAvatar(
              backgroundColor: Colors.indigo,
              child: Text('SL'),
            ),
          )
        ],
        
      ),
      body: const Center(
         child: CircleAvatar(
           maxRadius: 150,
           backgroundImage: NetworkImage("https://p4.wallpaperbetter.com/wallpaper/50/174/552/lana-rhoades-pornstar-blue-eyes-women-with-shades-wallpaper-preview.jpg",),
         ),
      ),
    );
  }
}