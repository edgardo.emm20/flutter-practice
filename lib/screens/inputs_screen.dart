import 'package:flutter/material.dart';
import 'package:flutter_new_ver/widgets/widgets.dart';

class InputsScreen extends StatelessWidget {

  const InputsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final GlobalKey<FormState> myFormKey = GlobalKey();

    final Map<String, String> formValues = {
      'firstName': 'Fernando',
      'lastName': 'Herrera',
      'email': 'fer@gmail.com',
      'password': '123456',
      'role': 'Admin'
    };

    return Scaffold(
      appBar: AppBar(
        title: const Text("Inputs y Forms"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Form(
            key: myFormKey,
            child: Column(
              children:[
                CustomInputField(
                  hintText: 'Nombre del usuario',
                  labelText: 'Nombre',
                  formProperty: 'first_name',
                  formValues: formValues,
                ),
                const SizedBox(height: 30,),
                CustomInputField(
                  hintText: 'Apellido del usuario',
                  labelText: 'Apellido',
                  formProperty: 'last_name',
                  formValues: formValues,
                ),
                const SizedBox(height: 30,),
                CustomInputField(
                  hintText: 'Correo del usuario',
                  labelText: 'Correo',
                  inputType: TextInputType.emailAddress,
                  formProperty: 'email',
                  formValues: formValues,
                ),
                const SizedBox(height: 30,),
                CustomInputField(
                  hintText: 'Password',
                  labelText: 'Password',
                  isPassword: true,
                  formProperty: 'password',
                  formValues: formValues,
                ),
                const SizedBox(height: 30,),
                DropdownButtonFormField<String>(
                  items: const [
                    DropdownMenuItem(
                      child: Text("ADMIN"),
                      value: 'Admin',
                    ),
                    DropdownMenuItem(
                      child: Text("AEA"),
                      value: 'Aea',
                    )
                  ],
                  onChanged: (value) {
                    formValues['role'] = value ?? 'Admin';
                  },
                ),
                const SizedBox(height: 30,),
                ElevatedButton(
                  child: const SizedBox(
                    width: double.infinity,
                    child: Text('Guardar'),
                  ),
                  onPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    if(!myFormKey.currentState!.validate()) {
                      print('Formulario no valido');
                      return;
                    }
                    print(formValues);
                  },
                )
          
              ],
            ),
          ),
        ),
      )
    );
  }
}

