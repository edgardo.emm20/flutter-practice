export 'package:flutter_new_ver/screens/alert_screen.dart';
export 'package:flutter_new_ver/screens/card_screen.dart';
export 'package:flutter_new_ver/screens/listview1_screen.dart';
export 'package:flutter_new_ver/screens/listview2_screen.dart';

export 'package:flutter_new_ver/screens/home_screend.dart';
export 'package:flutter_new_ver/screens/avatar_screen.dart';
export 'package:flutter_new_ver/screens/animated_screen.dart';

export 'package:flutter_new_ver/screens/inputs_screen.dart';


export 'package:flutter_new_ver/screens/slider_screen.dart';
export 'package:flutter_new_ver/screens/listview_builder_screen.dart';
