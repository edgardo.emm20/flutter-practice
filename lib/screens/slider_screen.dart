import 'package:flutter/material.dart';
import 'package:flutter_new_ver/theme/app_theme.dart';

class SliderScreen extends StatefulWidget {
   
  const SliderScreen({Key? key}) : super(key: key);

  @override
  State<SliderScreen> createState() => _SliderScreenState();
}

class _SliderScreenState extends State<SliderScreen> {

  double _sliderValue = 100;
  bool _sliderEnabled = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Slider & Checks"),
      ),
      body: Column(
        children: [
          Slider.adaptive(
            min: 50,
            max: 400,
            activeColor: AppTheme.primary,
            divisions: 10,
            value: _sliderValue,
            onChanged: _sliderEnabled ? (value) {
              _sliderValue = value;
              setState(() {});
            } : null,
          ),
          Checkbox(value: _sliderEnabled, onChanged: (value) {
            _sliderEnabled = value ?? true;
            setState(() {});
          },),
          CheckboxListTile(
            activeColor: AppTheme.primary,
            title: const Text("Slider Habilitar"),
            value: _sliderEnabled,  
            onChanged: (value) {
            _sliderEnabled = value ?? true;
            setState(() {});
          },),
          Switch(value: _sliderEnabled, onChanged: (value) {
            _sliderEnabled = value;
            setState(() {});
          }),
          SwitchListTile.adaptive(
            title: const Text("Habilitar swits"),
            value: _sliderEnabled,
            onChanged: (value) {
            _sliderEnabled = value;
            setState(() {});
          }),
          Expanded(
            child: SingleChildScrollView(
              child: Image(
                image: const NetworkImage("https://www.xtrafondos.com/wallpapers/vertical/astronauta-perdido-en-el-espacio-5498.jpg",),
                fit: BoxFit.contain,
                width: _sliderValue,
              ),
            ),
          ),
        ],
      )
    );
  }
}