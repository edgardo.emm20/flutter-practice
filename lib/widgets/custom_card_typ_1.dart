import 'package:flutter/material.dart';
import 'package:flutter_new_ver/theme/app_theme.dart';

class CustomCardType1 extends StatelessWidget {
  const CustomCardType1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          const ListTile(
            title: Text("soy un titulo"),
            leading: Icon(Icons.phone_android, color: AppTheme.primary),
            subtitle: Text('loremip sum aea manito '),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                  onPressed: () {},
                  child: const Text("Cancel")
                ),
                TextButton(
                  onPressed: () {},
                  child: const Text("OKS")
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}