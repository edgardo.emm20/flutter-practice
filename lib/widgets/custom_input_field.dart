import 'package:flutter/material.dart';

class CustomInputField extends StatelessWidget {

  final String? hintText;
  final String? labelText;
  final String? helperText;
  final IconData? icon;
  final IconData? suffixIcon;
  final TextInputType? inputType;
  final bool? isPassword;

  final String formProperty;
  final Map<String, String> formValues;

  const CustomInputField({
    Key? key, 
    this.hintText, 
    this.labelText,
    this.helperText, 
    this.icon,
    this.suffixIcon,
    this.inputType,
    this.isPassword = false, 
    required this.formProperty, 
    required this.formValues
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: inputType,
      initialValue: '',
      textCapitalization: TextCapitalization.words,
      obscureText: isPassword!,
      onChanged: (value) {
        formValues[formProperty] = value;
      },
      validator: (value) {
        if (value == null) return 'Este campo es requerido';
        return value.length < 3 ? 'Minimo de 3 letras' : null;
      },
      autovalidateMode: AutovalidateMode.onUserInteraction,
      decoration: InputDecoration(
        hintText: hintText ?? 'Nombre del usuario',
        labelText: labelText ?? 'Nombre',
        helperText: helperText ?? 'Sólo letras',
        suffixIcon: Icon(suffixIcon),
        icon: icon != null ? Icon(icon): null,
            
      ),

    );
  }
}