import 'package:flutter/material.dart';
import 'package:flutter_new_ver/models/menu_option.dart';
import 'package:flutter_new_ver/screens/screens.dart';

class AppRoutes {

  static final menuOptions = <MenuOption>[
    // MenuOption(route: 'home', icon: Icons.ac_unit_outlined, name: 'Home Screen', screen: const HomeScreen()),
    MenuOption(route: 'listview1', icon: Icons.ac_unit_outlined, name: 'List View 1', screen: const Listview1Screen()),
    MenuOption(route: 'listview2', icon: Icons.ac_unit_outlined, name: 'List View 2', screen: const Listview2Screen()),
    MenuOption(route: 'alert', icon: Icons.ac_unit_outlined, name: 'Alert - Alerts', screen: const AlertScreen()),
    MenuOption(route: 'card', icon: Icons.ac_unit_outlined, name: 'Cards', screen: const CardScreen()),
    MenuOption(route: 'avatar', icon: Icons.supervised_user_circle, name: 'AVatar', screen: const AvatarScreen()),
    MenuOption(route: 'animated', icon: Icons.supervised_user_circle, name: 'Animated screen', screen: const AnimatedScreen()),
    MenuOption(route: 'inputs', icon: Icons.input_outlined, name: 'TextInputs', screen: const InputsScreen()),
    MenuOption(route: 'slider', icon: Icons.sledding_outlined, name: 'Slider Screen', screen: const SliderScreen()),
    MenuOption(route: 'builder', icon: Icons.indeterminate_check_box, name: 'InfiniteScroll', screen: const ListViewBuilderScreen()),
  ];

  static Map<String, Widget Function(BuildContext)> getAppRoutes() {
    Map<String, Widget Function(BuildContext)> aux = {};
    aux['home'] = (_) => const HomeScreen();
    for (final element in menuOptions) {
      aux[element.route] = (_) => element.screen; 
    }
    return aux;
  }

  static const initialRoute = 'home';

  // static Map<String, Widget Function(BuildContext)> routes = {
  //   'listview1': (_) => const Listview1Screen(),
  //   'listview2': (_) => const Listview2Screen(),
  //   'alert': (_) => const AlertScreen(),
  //   'card': (_) => const CardScreen(),
  //   'home': (_) => const HomeScreen(),
  // };

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    return MaterialPageRoute(builder: (context) {
      return const AlertScreen();
    },);
  }

}